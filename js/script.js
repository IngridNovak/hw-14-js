'use strict';
let btn = document.getElementById('changeTheme');
let nightTheme = localStorage.getItem('NightTheme');
console.log(nightTheme);
if(nightTheme === 'true') {
    btn.innerText ='Day'
    let all = document.getElementsByTagName("*");
    let allList = Array.prototype.slice.call(all);
    allList.forEach(item => {
        if (item.classList) {
            item.classList.toggle('night_theme');
        }
    })
}

btn.addEventListener('click', setNightTheme);


function setNightTheme(e){
    if(btn.innerText ==='Day'){
        console.log(3);
        btn.innerText='Night';
        localStorage.setItem('NightTheme', 'false');
    }else{
        console.log(4)
        btn.innerText = 'Day';
        localStorage.setItem('NightTheme', 'true');
    }
    let all = document.getElementsByTagName("*");
    let allList = Array.prototype.slice.call(all);
    allList.forEach(item =>{
        if(item.classList){
            item.classList.toggle('night_theme');
        }
    })
}
